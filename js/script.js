$(function () {

    //クリックできる個数を入れる
    var count = 5;

    var a =[];

    for(var i = 1; i < count; i++){
        a.push(0);
    }
    console.log('ループが終わったとき' + i);

    $('.box').click(function(){
        console.log('クリックされたとき' + i);
        var m = $(this).attr('class');
        var n = m.replace('box box','');
        //クリックを記録、全部クリック済みかをチェック
        check(n - 1);
        return false;
    });

    //クリックされるごとに記録して、すべてクリック済みかをチェックする
    function check(number){
        //number番目がクリックされたことを記録
        a[number] = true;

        var ok = true;
        for(var i=0; i<a.length; i++) {
            //1個でもfalse(クリックされていない)であればokをfalseにする
            if(a[i]==false){
                ok = false;
                break;
            }
        }
        //okがtrueのまま(クリックされてないものは存在しない)であれば正解
        if(ok==true) $('.message').html('正解です');

    }




});
